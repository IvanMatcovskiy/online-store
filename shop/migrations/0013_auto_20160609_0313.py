# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0012_payment_user'),
    ]

    operations = [
        migrations.RenameField(
            model_name='article',
            old_name='priece',
            new_name='price',
        ),
        migrations.RenameField(
            model_name='product',
            old_name='min_priece',
            new_name='min_price',
        ),
    ]
