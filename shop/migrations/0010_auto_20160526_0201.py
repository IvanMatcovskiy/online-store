# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0009_auto_20160526_0036'),
    ]

    operations = [
        migrations.RenameField(
            model_name='article',
            old_name='product',
            new_name='id_product',
        ),
    ]
