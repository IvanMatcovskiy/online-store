# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0013_auto_20160609_0313'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cart_item',
            name='article',
        ),
        migrations.RemoveField(
            model_name='cart_item',
            name='payment',
        ),
        migrations.DeleteModel(
            name='Cart_item',
        ),
    ]
