# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0004_remove_product_quantity'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='category',
            field=models.CharField(max_length=20, choices=[('Rings', 'Rings'), ('Necklaces & Pendants', 'Necklaces & Pendants'), ('Earrings', 'Earrings'), ('Chains', 'Chains'), ('Bracelets', 'Bracelets'), ('Necklace', 'Necklace'), ('Brooches & Pins', 'Brooches & Pins'), ('Cufflinks', 'Cufflinks'), ('Clamps for tie', 'Clamps for tie')]),
        ),
        migrations.AlterField(
            model_name='product',
            name='insertion',
            field=models.CharField(max_length=9, choices=[('Diamonds', 'Diamonds'), ('Phianits', 'Phianits'), ('Pearl', 'Pearl'), ('Emeralds', 'Emeralds'), ('Rubies', 'Rubies'), ('Sapphires', 'Sapphires')]),
        ),
        migrations.AlterField(
            model_name='product',
            name='metal',
            field=models.CharField(max_length=8, choices=[('Gold', 'Gold'), ('Platinum', 'Platinum'), ('Silver', 'Silver'), ('Steel', 'Steel')]),
        ),
    ]
