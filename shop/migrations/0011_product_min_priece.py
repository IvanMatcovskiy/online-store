# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0010_auto_20160526_0201'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='min_priece',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
