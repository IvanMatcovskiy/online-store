# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0002_auto_20160524_2249'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Cart',
        ),
        migrations.DeleteModel(
            name='Category',
        ),
        migrations.DeleteModel(
            name='Choice',
        ),
        migrations.RemoveField(
            model_name='article',
            name='vendor_code',
        ),
        migrations.RemoveField(
            model_name='cart_item',
            name='id_cart_item',
        ),
        migrations.RemoveField(
            model_name='cart_item',
            name='total_cost',
        ),
        migrations.RemoveField(
            model_name='payment',
            name='id_cart',
        ),
        migrations.RemoveField(
            model_name='product',
            name='id_product',
        ),
        migrations.AddField(
            model_name='article',
            name='product',
            field=models.ForeignKey(to='shop.Product', default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='article',
            name='remaining_quantity',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='article',
            name='size',
            field=models.CharField(default='', max_length=2),
        ),
        migrations.AddField(
            model_name='cart_item',
            name='article',
            field=models.ForeignKey(to='shop.Article', default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='cart_item',
            name='payment',
            field=models.ForeignKey(to='shop.Payment', default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='category',
            field=models.CharField(choices=[('Rings', 'Rings'), ('Necklaces & Pendants', 'Necklaces & Pendants'), ('Earrings', 'Earrings'), ('Chains', 'Chains'), ('Bracelets', 'Bracelets'), ('Necklace', 'Necklace'), ('Brooches & Pins', 'Brooches & Pins'), ('Cufflinks', 'Cufflinks'), ('Clamps for tie', 'Clamps for tie')], default='Rings', max_length=20),
        ),
        migrations.AlterField(
            model_name='article',
            name='priece',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='article',
            name='weight_metal',
            field=models.CharField(default='', max_length=10),
        ),
        migrations.AlterField(
            model_name='cart_item',
            name='quantity',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='payment',
            name='cost',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='product',
            name='image',
            field=models.FilePathField(path='/home/ivan/djangogirls/shop/media'),
        ),
        migrations.AlterField(
            model_name='product',
            name='insertion',
            field=models.CharField(choices=[('Diamonds', 'Diamonds'), ('Phianits', 'Phianits'), ('Pearl', 'Pearl'), ('Emeralds', 'Emeralds'), ('Rubies', 'Rubies'), ('Sapphires', 'Sapphires')], default='Diamonds', max_length=9),
        ),
        migrations.AlterField(
            model_name='product',
            name='metal',
            field=models.CharField(choices=[('Gold', 'Gold'), ('Platinum', 'Platinum'), ('Silver', 'Silver'), ('Steel', 'Steel')], default='Gold', max_length=8),
        ),
        migrations.AlterField(
            model_name='product',
            name='name',
            field=models.CharField(default='', max_length=100),
        ),
        migrations.AlterField(
            model_name='product',
            name='quantity',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='product',
            name='weight_insertion',
            field=models.CharField(default='', max_length=10),
        ),
    ]
