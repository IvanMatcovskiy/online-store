from django.db import models
from django.utils import timezone
class Product(models.Model):
    CATEGORIES = (
        ('Rings', 'Rings'),
        ('Necklaces & Pendants', 'Necklaces & Pendants'),
        ('Earrings', 'Earrings'),
        ('Chains', 'Chains'),
        ('Bracelets', 'Bracelets'),
        ('Necklace', 'Necklace'),
        ('Brooches & Pins', 'Brooches & Pins'),
        ('Cufflinks', 'Cufflinks'),
        ('Clamps for tie', 'Clamps for tie'),
    )
    category = models.CharField(max_length=20, choices=CATEGORIES)
    name = models.CharField(max_length=100, default='')
    image = models.ImageField(upload_to="images/")
    METALS = (
        ('Gold', 'Gold'),
        ('Platinum', 'Platinum'),
        ('Silver', 'Silver'),
        ('Steel', 'Steel'),
    )
    def __str__(self):
        return self.image.url
    metal = models.CharField(max_length=8, choices=METALS)
    INSERTIONS = (
        ('Diamonds', 'Diamonds'),
        ('Phianits', 'Phianits'),
        ('Pearl', 'Pearl'),
        ('Emeralds', 'Emeralds'),
        ('Rubies', 'Rubies'),
        ('Sapphires', 'Sapphires'),
    )
    insertion = models.CharField(max_length=9, choices=INSERTIONS)
    weight_insertion = models.CharField(max_length=10, default='')
    min_price = models.PositiveIntegerField(default=0)
    def __str__(self):
        return str(self.id)
class Article(models.Model):
    size = models.CharField(max_length=2, default='')
    weight_metal = models.CharField(max_length=10, default='')
    price = models.PositiveIntegerField(default=0)
    remaining_quantity = models.PositiveIntegerField(default=0)
    id_product = models.ForeignKey('Product')
    def __str__(self):
        return str(self.id)
class Payment(models.Model):
    payment_type = models.CharField(max_length=200)
    date = models.DateTimeField()
    cost = models.PositiveIntegerField(default=0)
    user = models.ForeignKey('auth.User')