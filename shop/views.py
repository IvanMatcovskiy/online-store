from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.core.urlresolvers import reverse
from paypal.standard.forms import PayPalPaymentsForm
from .models import Product
from .models import Article, Payment
from cart.cart import Cart
import json
from django.core.serializers.json import DjangoJSONEncoder
from django.core.cache import cache
import time
import datetime
def main(request):
    
    l = ['Rings', 'Necklaces & Pendants', 'Earrings', 'Chains', 'Bracelets', 'Necklace', 'Brooches & Pins', 'Cufflinks', 'Clamps for tie']
    
    cache.get_or_set('products', Product.objects.all())
    amount = len(cache.get_or_set('products', Product.objects.all()))
    for product in cache.get_or_set('products', Product.objects.all()):
        article = Article.objects.filter(id_product=product).order_by("size")[0]
        product.min_price = article.price
        product.save()
    info = {
    'kinds' : l,
    'products' : cache.get_or_set('products', Product.objects.all()),
    'amount' : amount,
    'total_price' : Cart(request).summary()
    }
    return render(request, 'shop/StartPage.html', info)


def signin(request):
    return render(request, 'shop/SignIn.html')

def signup(request):
        return render(request, 'shop/SignUp.html')

@login_required
def profile(request):
    #Only for logged in users
    return render(request, 'shop/Profile.html')

def item(request, product_id):
    product = Product.objects.get(pk=product_id)
    articles = Article.objects.filter(id_product=product_id).order_by('size')
    item = articles[0]
    return render(request, 'shop/Item.html', {'product' : product, 'articles' : articles, 'item' : item, 'total_price' : Cart(request).summary()})

def bag(request):
    cart = []
    for elem in Cart(request):
        prod = Product.objects.get(pk=str(elem.product.id_product))
        cart.append((prod, elem.product))
    return render(request, 'shop/Bag.html', {'cart' : cart, 'total_price' : Cart(request).summary()})

def signout(request):
    logout(request)
    return redirect('/')

def add_to_cart(request):

    article_id = request.POST.get('article_id')[0]
    
    article = Article.objects.get(pk=article_id)
    print(article.price)
    cart = Cart(request)
    quantity = 1
    cart.add(article, article.price, quantity)
    return HttpResponse(cart.summary())

def remove_from_cart(request):
    article_id = request.POST.get('article_id')[0]
    article = Article.objects.get(pk=article_id)
    cart = Cart(request)
    cart.remove(article)
    return HttpResponse(cart.summary())

def size(request):
    article_id = request.POST.get('article_id')[0]
    
    article = Article.objects.get(pk=article_id)
    data = {'price' : article.price, 'weight_metal' : article.weight_metal}
    return HttpResponse(json.dumps(data, cls=DjangoJSONEncoder))


@csrf_exempt
def paypal_success(request):
    #Tell user we got the payment
    cart = Cart(request)
    purchase = Payment(payment_type="success", date=datetime.date.today(), cost=cart.summary(), user=request.user)
    purchase.save()
    cart.clear()
    return HttpResponse("Money is mine. Thanks.")


@login_required
def paypal_pay(request):
    #Page where we ask user to pay with paypal.
    cart = Cart(request)
    items = list()
    for elem in cart:
        prod = Product.objects.get(pk=str(elem.product.id_product))
        items.append(prod.name)
    paypal_dict = {
        "business": "matcovskiy.ivan-facilitator@gmail.com",
        "amount": cart.summary(),
        "currency_code": "RUB",
        "item_name": items,
        "invoice": "INV-" + str(int(time.time())),
        "notify_url": reverse('paypal-ipn'),
        "return_url": "http://localhost:8000/payment/success/",
        "cancel_return": "http://localhost:8000/payment/cart/",
        "custom": str(request.user.id),
    }
    # Create the instance.
    form = PayPalPaymentsForm(initial=paypal_dict)
    context = {"form": form, "paypal_dict": paypal_dict}
    return render(request, "shop/payment.html", context) 