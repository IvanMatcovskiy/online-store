from django.conf.urls import include, url,patterns
from . import views
import social.apps.django_app.urls
import paypal.standard.ipn.urls

from django.conf.urls import patterns
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [

    url(r'^$', views.main, name='main'),
    #url(r'^item/$', views.item, name='Item'),
    url(r'^item/(?P<product_id>[0-9]+)/$', views.item, name='item'),
    url(r'^bag/$', views.bag, name='Bag'),

    url(r'^signin/$', views.signin, name='Signin'),
    url(r'^signup/$', views.signup, name='Signup'),
    url(r'^signout/$', views.signout, name='signout'),
    url(r'^profile/$', views.profile, name='profile'),
    
    url(r'^add_to_cart/$', views.add_to_cart, name='add_to_cart'),
    url(r'^size/$', views.size, name='size'),
    url(r'^remove_from_cart/$', views.remove_from_cart, name='remove_from_cart'),

    url(r'^payment/cart/$', views.paypal_pay, name='cart'),
    url(r'^payment/success/$', views.paypal_success, name='success'),
    url(r'^paypal/', include(paypal.standard.ipn.urls)),
    url('', include(social.apps.django_app.urls, namespace='social')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)